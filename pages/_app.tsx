import { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";

import Layout from "@/components/layout";
import client from "@/services/config";

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <ApolloProvider client={client}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
  );
};

export default MyApp;
