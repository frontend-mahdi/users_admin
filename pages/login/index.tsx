import { useTheme } from "@mui/material/styles";
import { Typography, Box, Paper, Container } from "@mui/material";

import UserLoginForm from "@/components/UserLoginForm";

const SignIn = () => {
  const theme = useTheme();

  return (
    <>
      <Typography component="h1" variant="h5" marginY={2}>
        Sign in
      </Typography>
      <UserLoginForm />
    </>
  );
};
export default SignIn;
