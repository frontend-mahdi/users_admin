import { GET_USERS } from "@/services/queries/user";
import { useQuery } from "@apollo/client";
import Container from "@mui/material/Container";

import UsersAccordions from "@/components/Users";
import { Alert, Box, CircularProgress } from "@mui/material";

type Props = {};

const Users = (props: Props) => {
  const { loading, error, data } = useQuery(GET_USERS);
  console.log("data", data);

  if (loading)
    return (
      <Box sx={{ display: "flex" }}>
        <CircularProgress />
      </Box>
    );
  if (error)
    return (
      <Container>
        <Alert severity="error">Something went wrong...!</Alert>
      </Container>
    );

  return (
    <Container>
      <UsersAccordions data={data} />
    </Container>
  );
};

export default Users;
