import { useRouter } from "next/router";
import { Box, Button, Typography } from "@mui/material";

const NotFound = () => {
  const router = useRouter();
  return (
    <Box>
      <Typography textAlign="center" fontWeight="800" fontSize={45}>
        404
      </Typography>
      <Typography textAlign="center" marginBottom={2}>
        page not found!
      </Typography>
      <Button variant="text" onClick={() => router.push("/")}>
        go to main page
      </Button>
    </Box>
  );
};

export default NotFound;
