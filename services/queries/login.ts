import { gql } from "@apollo/client";

export const SIGN_IN = gql`
  mutation signIn($authUserInput: AuthUserInput!) {
    login(authUserInput: $authUserInput) {
      access_token
    }
  }
`;

export type SigInType = {
  login: {
    access_token: string;
  };
};
