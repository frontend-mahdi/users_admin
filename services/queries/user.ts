import { gql } from "@apollo/client";

export const GET_USERS = gql`
  query getUsers {
    users {
      results {
        name
        id
      }
      total
    }
  }
`;
export type GetUsersType = {
  users: {
    results: {
      name: string | null;
      id: string;
    }[];
    total: number;
  };
};
