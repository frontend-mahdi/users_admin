import { gql } from "@apollo/client";

export const GET_ROLES = gql`
  query userRoles($userId: String!) {
    userRoles(id: $userId) {
      id
      roles
    }
  }
`;
export type GetUserRolesType = {
  userRoles: {
    id: string;
    roles: UserRoles[];
  };
};

export enum UserRoles {
  "ADMIN",
  "CONTENT_EXPERT",
  "CONTENT_MANAGER",
  "COURIER",
  "CUSTOMER",
  "MAINTAINER",
  "SALES_EXPERT",
  "SALES_MANAGER",
}
