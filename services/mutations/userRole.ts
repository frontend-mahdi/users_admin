import { gql } from "@apollo/client";

export const ASSIGN_ROLE = gql`
  mutation AssignRole($assignRoleInput: EditRoleInput!) {
    assignRole(assignRoleInput: $assignRoleInput) {
      roles
    }
  }
`;
export const REMOVE_ROLE = gql`
  mutation RemoveRole($removeRoleInput: EditRoleInput!) {
    removeRole(removeRoleInput: $removeRoleInput)
  }
`;
