import { FC } from "react";

import { FormControlLabel, TextField, TextFieldProps } from "@mui/material";
import { Controller, useFormContext } from "react-hook-form";

interface ComponentType {
  label: string;
  name: string;
}
const TextInput: FC<ComponentType & TextFieldProps> = ({
  label,
  name,
  ...textProps
}) => {
  const { control } = useFormContext();

  return (
    <Controller
      control={control}
      name={name}
      defaultValue=""
      render={({ field: { onChange, value, ...field } }) => (
        <FormControlLabel
          control={
            <TextField
              fullWidth
              label={label}
              autoFocus
              autoComplete="off"
              margin="dense"
              {...textProps}
              onChange={onChange}
              value={value}
            />
          }
          label={""}
          sx={{ margin: 0 }}
          {...field}
        />
      )}
    />
  );
};

export default TextInput;
