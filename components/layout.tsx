import {
  FC,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";

import Box from "@mui/material/Box";
// import * as locales from "@mui/material/locale";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import {
  Button,
  Container,
  CssBaseline,
  IconButton,
  Paper,
  Stack,
} from "@mui/material";
import { ThemeProvider, createTheme, useTheme } from "@mui/material/styles";
import { useRouter } from "next/router";

type Component = {
  children: ReactNode;
};

const ColorModeContext = createContext({
  toggleColorMode: () => {},
  // localeMode: { toggleLocaleMode: () => {}, locale: "enUS" },
});

const layout: FC<Component> = ({ children }) => {
  const [mode, setMode] = useState<"light" | "dark">("light");
  // const [locale, setLocale] = useState<"enUS" | "faIR">("enUS");
  useEffect(() => {
    if (typeof window !== "undefined" && window.localStorage) {
      const baseMode = localStorage.getItem("theme") as
        | "light"
        | "dark"
        | undefined;
      if (baseMode === "light" || baseMode === "dark") setMode(baseMode);
    }
  }, []);
  const colorMode = useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
        localStorage.setItem("theme", mode === "light" ? "dark" : "light");
      },
    }),
    [mode]
  );
  // const localeMode = useMemo(
  //   () => ({
  //     toggleLocaleMode: () => {
  //       setLocale((prevMode) => (prevMode === "enUS" ? "faIR" : "enUS"));
  //     },
  //     locale,
  //   }),
  //   []
  // );

  const theme = useMemo(
    () =>
      createTheme(
        {
          palette: {
            mode,
          },
        }
        // locales[locale]
      ),
    [mode]
  );
  return (
    <>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Header />
          <Layout> {children}</Layout>
        </ThemeProvider>
      </ColorModeContext.Provider>
    </>
  );
};

export default layout;
function Layout({ children }: Component) {
  const theme = useTheme();
  return (
    <Container component="main">
      <Paper
        variant="outlined"
        component="section"
        sx={{
          p: 2,
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            py: 4,
            bgcolor: theme.palette.mode === "dark" ? "#2e2e2e" : "#f5f5f5",
          }}
        >
          {children}
        </Box>
      </Paper>
    </Container>
  );
}
function Header() {
  const theme = useTheme();
  const router = useRouter();
  const colorMode = useContext(ColorModeContext);
  const [loggedIn, setLoggedIn] = useState<boolean>(false);
  const logOutHandler = () => {
    localStorage.removeItem("token");
    router.push("/login");
  };
  useEffect(() => {
    if (typeof window !== "undefined" && window.localStorage) {
      const Token = localStorage.getItem("token");
      if (Token) setLoggedIn(true);
      else setLoggedIn(false);
    }
  });
  return (
    <Container>
      <Stack
        direction="row-reverse"
        justifyContent="space-between"
        paddingY={2}
      >
        <Box
          sx={{
            bgcolor: "background.default",
            color: "text.primary",
            borderRadius: 1,
            direction: "rtl",
          }}
        >
          <IconButton onClick={colorMode.toggleColorMode} color="inherit">
            {theme.palette.mode === "dark" ? (
              <Brightness7Icon />
            ) : (
              <Brightness4Icon />
            )}
          </IconButton>
          {theme.palette.mode} mode
        </Box>
        <Box>
          <Button onClick={() => router.back()}>Go Back</Button>
          {loggedIn && (
            <Button variant="contained" color="error" onClick={logOutHandler}>
              Log Out
            </Button>
          )}
        </Box>
      </Stack>
      {/* <Box
        sx={{
          display: "flex",
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          bgcolor: "background.default",
          color: "text.primary",
          borderRadius: 1,
          p: 3,
        }}
      >
        lang
        <IconButton onClick={localeMode.toggleLocaleMode} color="inherit">
          {localeMode.locale === "enUS" ? (
            <Typography>English</Typography>
          ) : (
            <Typography>فارسی</Typography>
          )}
        </IconButton>
      </Box> */}
    </Container>
  );
}
