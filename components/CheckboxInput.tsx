import { FC } from "react";

import { Controller, useFormContext } from "react-hook-form";
import { Checkbox, CheckboxProps, FormControlLabel } from "@mui/material";

type ComponentType = {
  label: string;
  name: string;
};
const CheckboxInput: FC<ComponentType & CheckboxProps> = ({
  label,
  name,
  ...inputProps
}) => {
  const { control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={false}
      render={({ field: { onChange, value, ...field } }) => (
        <FormControlLabel
          control={
            <Checkbox onChange={onChange} checked={value} {...inputProps} />
          }
          label={label}
          {...field}
        />
      )}
    />
  );
};

export default CheckboxInput;
