import { SIGN_IN, SigInType } from "@/services/queries/login";
import { useMutation } from "@apollo/client";
import { Button, Stack } from "@mui/material";
import { FormProvider, useForm } from "react-hook-form";

import { useRouter } from "next/router";
import TextInput from "./TextInput";

type FormFields = {
  email: string;
  password: string;
};

const UserLoginForm = () => {
  const methods = useForm<FormFields>({ mode: "onSubmit" });
  const { handleSubmit } = methods;
  const [loginMe, { loading }] = useMutation<SigInType>(SIGN_IN);
  const router = useRouter();

  const handleSubmitForm = async (data: FormFields) => {
    console.log(data);
    try {
      const response = await loginMe({
        variables: {
          authUserInput: data,
        },
      });

      const Access = response.data?.login.access_token;
      if (Access) {
        localStorage.setItem("token", Access);
        router.push("/users");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <FormProvider {...methods}>
      <Stack
        component="form"
        direction="column"
        onSubmit={handleSubmit(handleSubmitForm)}
      >
        <TextInput name="email" label="Email Address" type="text" autoFocus />
        <TextInput name="password" label="Password" type="password" />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
          disabled={loading}
        >
          {!loading ? "Sign In" : "Loading ..."}
        </Button>
      </Stack>
    </FormProvider>
  );
};

export default UserLoginForm;
