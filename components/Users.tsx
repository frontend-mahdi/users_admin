import { FC, useState } from "react";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";

import { GetUsersType } from "@/services/queries/user";
import UserPermissionsForm from "./UserPermissionsForm";
type ComponentType = {
  data: GetUsersType;
};
const UsersAccordions: FC<ComponentType> = ({ data }) => {
  const [expanded, setExpanded] = useState<string | false>(false);
  const theme = useTheme();

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };
  console.log("data.users.results", data.users.results);
  let Result = [...data.users.results];
  return (
    <Box component="div">
      {Result.sort((a, b) => a.id.localeCompare(b.id)).map((item, index) => (
        <Accordion
          key={index}
          expanded={expanded === `panel${index}`}
          onChange={handleChange(`panel${index}`)}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls={`panel${index}bh-content`}
            id={`panel${index}bh-header`}
          >
            <Typography fontWeight={600}>User {index + 1}</Typography>
          </AccordionSummary>
          <AccordionDetails
            sx={{
              bgcolor: theme.palette.mode === "dark" ? "#2e2e2e" : "#ececec",
            }}
          >
            <UserPermissionsForm id={item.id} />
          </AccordionDetails>
        </Accordion>
      ))}
    </Box>
  );
};
export default UsersAccordions;
