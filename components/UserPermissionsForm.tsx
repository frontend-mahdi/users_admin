import { FC, useEffect } from "react";

import { useMutation, useQuery } from "@apollo/client";
import { Box, Button } from "@mui/material";
import { FormProvider, useForm } from "react-hook-form";

import { ASSIGN_ROLE, REMOVE_ROLE } from "@/services/mutations/userRole";
import {
  GET_ROLES,
  GetUserRolesType,
  UserRoles,
} from "@/services/queries/roles";

import CheckboxInput from "./CheckboxInput";

type FormFields = {
  [key in keyof typeof UserRoles]: boolean;
};

type ComponentType = {
  // data: RulesType[] | null;
  id: string;
};

const UserPermissionsForm: FC<ComponentType> = ({ id }) => {
  console.log("id", id);

  const { data } = useQuery<GetUserRolesType>(GET_ROLES, {
    variables: {
      userId: id,
    },
  });
  const [assignRole, { loading: loadingA }] = useMutation(ASSIGN_ROLE);
  const [removeRole, { loading: loadingB }] = useMutation(REMOVE_ROLE);

  const methods = useForm<FormFields>({
    mode: "onSubmit",
  });
  const { handleSubmit, reset } = methods;

  console.log(data);

  useEffect(() => {
    if (data) {
      let defaultValues: Partial<FormFields> = {};
      data.userRoles.roles.forEach((item) => {
        defaultValues[item] = true;
      });
      reset(defaultValues);
    }
  }, [data]);

  const handleSubmitForm = (data: FormFields) => {
    console.log(data);
    const trueRoles: (keyof typeof UserRoles)[] = [];
    const falseRoles: (keyof typeof UserRoles)[] = [];
    Object.keys(data).forEach((item) => {
      if (data[item as keyof typeof UserRoles]) {
        trueRoles.push(item as keyof typeof UserRoles);
      } else falseRoles.push(item as keyof typeof UserRoles);
    });

    trueRoles.forEach(async (item) => {
      try {
        const response = await assignRole({
          variables: {
            assignRoleInput: {
              userId: id,
              role: item,
            },
          },
        });

        console.log(response);
      } catch (error) {
        console.log(error);
      }
    });
    falseRoles.forEach(async (item) => {
      try {
        const response = await removeRole({
          variables: {
            removeRoleInput: {
              userId: id,
              role: item,
            },
          },
        });

        console.log(response);
      } catch (error) {
        console.log(error);
      }
    });
  };
  const resetFieldsHandler = () => {
    reset({
      ADMIN: false,
      CONTENT_EXPERT: false,
      CONTENT_MANAGER: false,
      COURIER: false,
      CUSTOMER: false,
      MAINTAINER: false,
      SALES_EXPERT: false,
      SALES_MANAGER: false,
    });
  };

  const PERMISSIONS = [
    "ADMIN",
    "CONTENT_EXPERT",
    "CONTENT_MANAGER",
    "COURIER",
    "CUSTOMER",
    "MAINTAINER",
    "SALES_EXPERT",
    "SALES_MANAGER",
  ];
  return (
    <FormProvider {...methods}>
      <Box component="form" onSubmit={handleSubmit(handleSubmitForm)}>
        {PERMISSIONS.map((item, index) => (
          <CheckboxInput key={index} label={item} name={item} />
        ))}

        <Box
          sx={{
            mt: 4,
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            gap: 2,
          }}
        >
          <Button
            variant="contained"
            type="submit"
            disabled={loadingA || loadingB}
          >
            {!loadingA && !loadingB ? "submit" : "loading ..."}
          </Button>
          <Button variant="outlined" onClick={resetFieldsHandler}>
            reset
          </Button>
        </Box>
      </Box>
    </FormProvider>
  );
};

export default UserPermissionsForm;
